import requests
import re
import time
import des_rsa
import execjs

class SSL:

    def __init__(self,user,pw):
        self.user = user
        self.pw = pw
        self.session = requests.Session()
    
    def login_vpn(self):
        headers1 = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.88 Safari/537.36',
        }
        vpndata = {
            'tz_offset': '480',
            'username': self.user,
            'password': self.pw,
            'realm': 'LDAP-User',
            'btnSubmit': '登 录'
        }
        vpnurl = 'https://sslvpn.bnu.edu.cn/dana-na/auth/url_default/login.cgi'
        r1 = self.session.post(url=vpnurl, data=vpndata, headers=headers1)
        xsauth_value = re.findall('value="(.*?)"', r1.text)
        post_data = {
            'xsauth': xsauth_value,
            'tz_offset': '480',
            'clienttime': str(int(time.time())),
            'activex_enabled': '0',
            'java_enabled': '1',
            'power_user': '0',
            'grab': '1',
            'check': 'yes'
        }
        r = self.session.post(url='https://sslvpn.bnu.edu.cn/dana/home/starter0.cgi', data=post_data, headers=headers1,
                         verify=False, )
        if self.user in r.text:
            print('VPN登录成功')
            return 0
        else:
            vpnymq_url = 'https://sslvpn.bnu.edu.cn/dana-na/auth/url_default/welcome.cgi?p=user%2Dconfirm'
            r = self.session.get(url=vpnymq_url, headers=headers1, verify=False)
            relogin = re.findall('FormDataStr" value="(.*?)">', r.text)
            #print(relogin)
            relogindata = {
                'btnContinue': '继续会话',
                'FormDataStr': relogin
            }
            r2 = self.session.post(url='https://sslvpn.bnu.edu.cn/dana-na/auth/url_default/login.cgi', data=relogindata,
                              headers=headers1, verify=False)
            if '信息门户密码' in r2.text:
                print('VPN好像登陆失败了')
                print(r2.text)
            elif 'You do not have permission to login' in r2.text:
                print('您目前的网络或许是校园网，请切换到校外网进行测试')
            else:
                print('VPN登陆成功，将下线其他VPN')

    def logout_vpn(self):
        url = 'https://sslvpn.bnu.edu.cn/dana-na/auth/logout.cgi?delivery=psal'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Mobile Safari/537.36',
        }
        r = self.session.get(url=url, headers=headers)
        if '退出VPN系统' in r.text:
            print('当前已退出VPN系统')
        else:
            print('未退出成功，或许遇到问题')
    
    # 登录数字京师
    def login_onebnu(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Mobile Safari/537.36'
        }
        r = self.session.get(
            'https://sslvpn.bnu.edu.cn/cas/,DanaInfo=cas.bnu.edu.cn+login?service=https%3A%2F%2Fcgyd.prsc.bnu.edu.cn%2Flogin.jsp',
            headers=headers, verify=False)
        lt = re.findall('id="lt" name="lt" value="(.*?)"', r.text)[0]
        execution = re.findall('name="execution" value="(.*?)"', r.text)[0]
        ctx = execjs.compile(des_rsa.f)
        rsa = ctx.call('strEnc', self.user + self.pw + lt, '1', '2', '3')
        login_data = {
            'rsa': rsa,
            'ul': len(self.user),
            'pl': len(self.pw),
            'lt': lt,
            'execution': execution,
            '_eventId': 'submit'
        }
        # print(login_data)
        url_post = 'https://sslvpn.bnu.edu.cn/cas/,DanaInfo=cas.bnu.edu.cn+login?service=https%3A%2F%2Fcgyd.prsc.bnu.edu.cn%2Flogin.jsp'
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Mobile Safari/537.36'
        }
        r = self.session.post(url_post, data=login_data, headers=headers, verify=False)
        if '北京师范大学统一身份认证' in r.text:
            print('ONEBNU账号登陆失败')
            return 0
        else:
            print('ONEBNU登陆成功')
            return 1
            
    def get_cache_data(self,date):
        url = 'https://sslvpn.bnu.edu.cn/gymsite/,DanaInfo=cgyd.prsc.bnu.edu.cn,SSL+cacheAction.do?ms=viewBook&gymnasium_id=2&item_id=5326&time_date='+ date+'&userType=1'
        r = self.session.get(url)
        return r.text