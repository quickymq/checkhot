# -*- coding: utf-8 -*-

import requests
import re
import numpy as np
import pandas as pd
import time
import datetime
import pytz
import ssl_login
import os
import logging
import traceback

class getymqdata:
    
    def __init__(self,user, pw , date='', runtime = 60):
            
        self.user = user
        self.pw = pw
        self.date = date
        if date == '':
            self.date = (datetime.datetime.utcnow() + datetime.timedelta(hours=80)).strftime('%Y-%m-%d') #80 小时为4天后的北京时间 24*3+8=80
        self.runtime = runtime
        # self.intervals = intervals
        self.session = requests.Session()
        self.myssl = ssl_login.SSL(self.user,self.pw)
        self.myssl.login_vpn()
        self.myssl.login_onebnu()
        self.path_header = r'/root/python_obj/checkhot/'
        self.target_time = datetime.datetime.now().replace(hour=7, minute=30, second=0, microsecond=0).timestamp()
        self.error_count = 0
        logging.basicConfig(filename=r'/root/python_obj/checkhot/log/app.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s') 
        print(str(datetime.datetime.now())+' -------任务启动--------')
        logging.info("-------任务启动--------")
    
    def setintervals(self):
        time_delta = time.time()-self.target_time
        if time_delta < 120:
            return 1
        elif time_delta < 300:
            return 2
        elif time_delta < 1800:
            return 30
        else:
            return 300
    
    def show_time(self):
        # 剩余时间显示
        time_left = self.runtime - (time.time()- self.target_time)
        interval_time = self.setintervals()
        if time_left > interval_time:
            return 1

            # # 按每秒显示时间 不宜用于存储
            # count = 0
            # for i in range(0,interval_time):
                # left_time = interval_time - count
                # count = count + 1
                # time_left = self.runtime - (time.time()- self.target_time)
                # print('\r本次循环剩余时间为：%.2f s，最长等待时间为：%.2f s           '%(left_time,time_left),end="") 
                # time.sleep(1)

        elif time_left < interval_time and time_left > 0:
            print('\r最后剩余时间为：%.2f s           '%time_left,end="")
            time.sleep(time_left)
            # # 按每秒显示时间 不宜用于存储
            # for i in range(int(time_left)):
                # time_left = self.runtime - (time.time()- self.target_time)
                # print('\r最后剩余时间为：%.2f s           '%time_left,end="") 
                # time.sleep(1)
            return 0
        else:
            print(f'设定的运行时间小于【当前时间-任务启动时间】，任务结束  {time_left}  ')
            return 0
    
    def get_statues_rawdata(self):
        data = self.myssl.get_cache_data(self.date)
        markp = re.findall("markResStatus\('\d+','(\d+)','\d'\)",data)
        markp = np.asarray(markp,dtype=int)
        all_place = re.findall("id:'(\d+)',time_session:",data)
        all_place = np.asarray(all_place,dtype=int)
        return markp,all_place
    
    def build_frame(self):
        df = pd.read_excel(self.path_header+'database.xlsx')
        if self.date not in df.columns:
            df[self.date] = None
        return df
    
    def filldata(self,dataframe,marked_place,whole_place):
        # 首先填充可选place
        if 'showed' not in dataframe.loc[:,'statues'].values and 'showed-booked' not in dataframe.loc[:,'statues'].values: # 未初始化
            for i in range(len(dataframe)):
                if dataframe.loc[i,'place_num'] in whole_place: # 判断是否可选
                    dataframe.loc[i,'statues'] = 'showed' 
        # 数据填充
        for i in range(len(dataframe)):
            if dataframe.loc[i,'statues'] != 'showed-booked' and dataframe.loc[i,'place_num'] in marked_place:
                dataframe.loc[i,'statues'] = 'showed-booked'
                dataframe.loc[i,self.date] = time.time()-self.target_time
        return dataframe
    
    def fill_blank_excel_and_build(self, data):
        file_path = self.path_header+f'result/result_{self.date}.xlsx'
        if os.path.exists(file_path):
            # 读取Excel文件为DataFrame
            df_excel = pd.read_excel(file_path)
            # 获取最后一列的数据 保存时间的数据
            last_column = df_excel.iloc[:, -1]
            # 检查最后一列的空单元格 输出布尔值
            empty_cells = last_column.isnull()
            # 用data dataframe最后一行填充空单元格
            last_column = last_column.fillna(data.iloc[:, -1])
            df_excel.iloc[:, -1] = last_column
            # 将倒数第二列设置为filldata中的Statues
            df_excel.loc[:, 'statues'] = data.loc[:,'statues']
            # 输出新的文档
            df_excel.to_excel(self.path_header+f'result/result_{self.date}.xlsx', index=False)
            print(f'已将更新的结果输出至result_{self.date}.xlsx')
        else:
            data.to_excel(self.path_header+f'result/result_{self.date}.xlsx', index=False)
            print(f'已建立新的文档，结果输出至result_{self.date}.xlsx')
        
    def get_result(self):
        try:
            df = self.build_frame()
            start_time = time.time()  # 获取当前时间
            while True:
                # 检查是否到达target_time [默认7:30]
                if self.target_time < time.time():
                    timedelta = time.time() - start_time
                    if timedelta > self.runtime:
                        print(f'Finished')
                        break
                    else:
                        if 'showed' not in df['statues'].values and 'showed-book' in df['statues'].values: # 只有uncheck和showed_bo* 则表示完成
                            print('The current venues are all booked and there are no vacant venues')
                            break
                        else:
                            try:
                                markp,all_place = self.get_statues_rawdata()
                                df = self.filldata(df,markp,all_place)
                                df = df.round({self.date:3})
                                if self.show_time() != 1:  # 时间处理函数
                                    break
                                else:
                                    self.fill_blank_excel_and_build(df) # 需要继续循环 先保存
                                    logging.info("已保存excel文档-循环中")
                                    print(f'即将睡眠{self.setintervals()} s')
                                    time.sleep(self.setintervals())
                            except requests.exceptions.ConnectionError as e:
                                print("ERROR:", e)
                                print('或许网络有问题，即将重新载入SSL')
                                logging.info("或许网络有问题，即将重新载入SSL")
                                self.myssl = ssl_login.SSL(self.user,self.pw)
                                self.myssl.login_vpn()
                                self.myssl.login_onebnu()
                                self.error_count = self.error_count + 1
                                time.sleep(1)
                                if self.error_count > 10:
                                    print('错误，已重试超过10次，即将退出')
                                    return None
                else: # 未到目标时间时
                    pass
            self.fill_blank_excel_and_build(df)
            logging.info("已保存excel文档-结束")
            self.myssl.logout_vpn()
            return 1
        except Exception as e:
            traceback.print_exc()
            logging.error(traceback.format_exc())
            logging.info("程序运行结束")
            return 0
            
# my_df = getymqdata()
# result = my_df.get_result()

