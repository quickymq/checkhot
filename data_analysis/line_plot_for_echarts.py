# -*- coding: utf-8 -*-


import os
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import json

def output_linedata(base_path):
    df_ft = pd.read_excel(base_path+'data_analysis/result_DataAnalysis.xlsx', sheet_name='result_raw')
    df_compare = []
    for column in df_ft.columns[1:]:
        column_data = df_ft[column]
        compare_min = column_data.loc[column_data >= 1].min()
        compare_max = column_data.loc[column_data >= 1].max()
        if compare_max == np.inf:
            compare_max = 4*60*60
        df_compare.append([column, compare_min,compare_max])
    df_result = pd.DataFrame(df_compare, columns=['Time', 'min_Value','max_Value'])
    df = df_result.copy()
    df['Time'] = pd.to_datetime(df['Time'])  # 将第一列转换为datetime类型
    df = df.sort_values(by='Time')  # 按时间顺序排序
    df = df.reset_index(drop=True)  # 重新索引
    # 处理Time
    df_time = df.loc[:,'Time'].dt.strftime('%Y-%m-%d')
    # edit content
    xdata =  df_time.tolist()
    ydata_fast = df.loc[:,'min_Value'].tolist()
    ydata_slow = df.loc[:,'max_Value'].tolist()
    # 将ydata_fast和ydata_slow保留一位小数
    ydata_fast = [round(val, 1) for val in ydata_fast]
    ydata_slow = [round(val, 1) for val in ydata_slow]
    output_data = {'xdata':xdata,'ydata_slow':ydata_slow,'ydata_fast':ydata_fast}
    # print(output_data)
    with open (base_path+r'result/line_plot_data.json','w') as f:
        f.write(json.dumps(output_data))
    print('已输出line_plot_for_echarts JSON文件')
        