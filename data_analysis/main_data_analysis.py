# -*- coding: utf-8 -*-
"""
Data Analysis
Heatmap源数据中， -1表示通过threshold筛率的无效数据，其他
"""
import numpy as np
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import logging


class data_analysis:
    
    def __init__(self,threshold_value=0,debug_mode='off'):
        
        self.threshold_value = threshold_value  # 过滤最小数值
        self.result_cal_score = None
        self.result = None
        self.score_matrix = None
        if debug_mode == 'on':
            self.path = ''
        else:
            self.path = r'/root/python_obj/checkhot/'
        self.debug = ''
        
    # 数据清洗
    def washdata(self,dataframe):
        condition1 = dataframe['statues'] != 'uncheck'
        filtered_df = dataframe[condition1]
        filtered_df = filtered_df.fillna(np.inf)
        # filtered_df = filtered_df.mask(filtered_df.loc[:,'statues'] == 'uncheck', np.nan)
        # condition2 = dataframe['statues'] == 'uncheck'
        # filtered_df = dataframe[condition2]
        # filtered_df = filtered_df.fillna(np.nan)
        
        return filtered_df
    
    # 得分计算
    def calculate_value(self,number):
        matrix = [
            (-10, 1, -1),
            (1, 2, 9.9),
            (2, 3, 9.8),
            (3, 4, 9.7),
            (4, 5, 9.6),
            (5, 6, 9.5),
            (6, 7, 9.4),
            (7, 8, 9.3),
            (8, 9, 9.2),
            (9, 10, 9.1),
            (10, 11, 9),
            (11, 12, 8.9),
            (12, 13, 8.8),
            (13, 14, 8.7),
            (14, 15, 8.6),
            (15, 30, 8.3),
            (30, 60, 7.5),
            (60, 120, 6.5),
            (120, 300, 5.5),
            (300, 600, 4.5),
            (600, 1800, 3.5),
            (1800, 3600, 2.5),
            (3600, 10800, 1.5),
            (10800, float('inf'), 0.5)
        ]
        for value_range in matrix:
            if value_range[0] <= number < value_range[1]:
                return value_range[2]
            elif np.isnan(number):
                return np.nan
        return 0.5
    
    def calculate_average(self,row):
        non_negative_values = row[1:][row[1:] >= 0]
        if non_negative_values.empty:
            result = 0
        else:
            result = non_negative_values.mean()
        return result
        
    def draw_hotmap(self):
        pn = ['y1','y2','y3','y4','y5','y6','x1','x2','x3','x4']
        tn = ['8-9','9-10','10-11','11-12','12-13','13-14','14-15','15-16','16-17','17-18','18-19','19-20','20-21','21-22']
        new_result_matrix = self.result_cal_score['mean_score'].values.reshape(int(len(self.result_cal_score['mean_score'])/10),10)
        sns.set(font_scale=1.2)  # 设置字体大小
        fig, ax = plt.subplots(figsize=(8, 6))
        sns.heatmap(new_result_matrix, annot=False, cmap='YlOrRd',cbar=True, ax=ax, vmin=0, vmax=10,xticklabels=pn, yticklabels=tn)
        ax.set_xlabel('PN')  # 设置X轴标签
        ax.set_ylabel('TIMELIST')  # 设置Y轴标签
        ax.set_title('Heatmap', fontdict={'fontsize': 16, 'fontweight': 'bold'})  # 设置标题字体大小和样式
        ax.text(0, -1, f'Data colloction range: {self.result.columns[1]} - {self.result.columns[-1]}, total {self.result.shape[1]-1} days', ha='left', fontsize=10)  # 添加底部文字
        plt.savefig(self.path+"data_analysis/result.png")
        logging.info("已输出result.png")
        # plt.show()
        return new_result_matrix
    
    def output_webdata(self):
        web_data = []
        for i in range(0,self.score_matrix.shape[0]):
            for j in range(0,self.score_matrix.shape[1]):
                web_data.append([i,j,self.score_matrix[i,j]])
        web_data = np.array(web_data)
        np.set_printoptions(precision=1, suppress=True)
        formatted_data = np.array2string(web_data, separator=', ', suppress_small=True)
        formatted_data = formatted_data.replace('. ','').replace(' ','')
        with open(self.path+'result/api_imgdata.txt', 'w') as file:
            file.write(formatted_data)

    def output_result(self):
        df_database = pd.read_excel(self.path+'database.xlsx')
        place_num = df_database['place_num']
        result_origin_data = df_database.loc[:,['place_num','place_time','place_name']]
        self.result = pd.DataFrame(place_num)
        # 获取文件列表
        file_names = os.listdir(self.path+'result')  # 读取文件列表
        fn = []
        for file_name in file_names:
            if '.xlsx' in file_name:
                fn.append(file_name)
        # 读取文件以及数据清洗
        for file_name in fn:
            df = pd.read_excel(self.path+f'result/{file_name}',engine='openpyxl')
            washed = self.washdata(df) 
            self.result = pd.concat([self.result, washed.iloc[:,-1]], axis=1)
            result_origin_data = pd.concat([result_origin_data, df.iloc[:,[-2,-1]]], axis=1)
        self.result_cal_score = self.result.copy()  # 复制原始DataFrame
        
        for i in range(1,self.result.shape[1]):
            self.result_cal_score.iloc[:, i] = self.result_cal_score.iloc[:, i].apply(self.calculate_value)  # 应用函数到第二列到最后一列的所有元素
        self.result_cal_score['mean_score'] = self.result_cal_score.apply(lambda row: self.calculate_average(row),axis=1)
        self.score_matrix = self.draw_hotmap()
        # 数据写入
        with pd.ExcelWriter(self.path+r'data_analysis/result_DataAnalysis.xlsx') as writer:
            self.result_cal_score.to_excel(writer, sheet_name='result_cal_score', index=False)
            self.result.to_excel(writer, sheet_name='result_raw', index=False)
            result_origin_data.to_excel(writer, sheet_name='result_origin_data', index=False)
        logging.info("数据分析完毕，已输出Excel文件")
        print("数据分析完毕，已输出Excel文件")
        self.output_webdata()
        print("已输出webdata文件")

if __name__ == '__main__':       
    da = data_analysis(threshold_value=1,debug_mode='on')
    a = da.output_result()
