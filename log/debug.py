import requests
import re
import numpy as np
import pandas as pd
import time
import datetime
import pytz

#%%
url = f'https://cgyd.prsc.bnu.edu.cn/gymsite/cacheAction.do?ms=viewBook&gymnasium_id=2&item_id=5326&time_date=2023-07-15&userType=1'
r = requests.get(url)
def get_statues_rawdata(r):
    markp = re.findall("markResStatus\('\d+','(\d+)','\d'\)",r.text)
    markp = np.asarray(markp,dtype=int)
    all_place = re.findall("id:'(\d+)',time_session:",r.text)
    all_place = np.asarray(all_place,dtype=int)
    return markp,all_place
markp,all_place = get_statues_rawdata(r)
#%%
# showed1 avaliable2 other0
dataframe = pd.read_excel('database.xlsx')
target_time = datetime.datetime.now().replace(hour=7, minute=30, second=0, microsecond=0).timestamp()
date = (datetime.datetime.utcnow() + datetime.timedelta(hours=80)).strftime('%Y-%m-%d')
for i in range(len(dataframe)):
    if dataframe.loc[i,'place_num'] in all_place:
        dataframe.loc[i,'statues'] = 'showed'
        if dataframe.loc[i,'statues'] != 'showed-booked' and dataframe.loc[i,'place_num'] in markp:
            dataframe.loc[i,'statues'] = 'showed-booked'
            dataframe.loc[i,date] = time.time()-target_time